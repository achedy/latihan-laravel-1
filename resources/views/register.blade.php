@extends('adminlte.master')

@section('title')
  Register
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item active">Register</li>
@endsection


@section('content')

  <form action="/welcome" method="post">
      @csrf
      <p>First Name:</p>
      <p><input type="text" name="firstname" /></p>

      <p>Last Name:</p>
      <p><input type="text" name="lastname" /></p>

      <p>Gender:</p>
      <p>
          <input type="radio" name="gender" value="male" /> Male <br />
          <input type="radio" name="gender" value="female" /> Female <br />
          
      </p>

      <p>Nationality</p>
      <p>
          <select name="nationality">
              <option value="Indonesia">Indonesia</option>
              <option value="Jepang">Jepang</option>
              <option value="Amerika">Amerika</option>
          </select>
      </p>

      <p>Language Spoken:</p>
      <p>
      <input type="checkbox" name="lang" value="1" /> Bahasa Indonesia<br />
      <input type="checkbox" name="lang" value="2" /> English<br />
      <input type="checkbox" name="lang" value="3" /> Other<br />
      </p>

      <p>Bio:</p>
      <p>
          <textarea name="bio" cols="30" rows="10"></textarea>
      </p>
      <p>
          <input type="submit" value="Sign Up" />
      </p>
  </form>    

@endsection