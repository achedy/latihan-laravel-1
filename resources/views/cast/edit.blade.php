@extends('adminlte.master')
@section('title')
Edit Cast
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item"><a href="/cast">Cast</a></li>
<li class="breadcrumb-item active">Edit Cast</li>
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" id="nama" value="{{$show->nama}}" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="number" class="form-control" name="umur" id="umur" value="{{$show->umur}}" placeholder="Masukkan Umur">
        @error('usia')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" name="bio" id="bio" placeholder="Masukkan Bio">{{$show->bio}}</textarea>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <input type="hidden" name="id" value="{{$show->id}}"  />
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection