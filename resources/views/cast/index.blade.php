@extends('adminlte.master')
@section('title')
  Master Cast
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item active">Master Cast</li>
@endsection

@section('content')
<a class="btn btn-success btn-sm mb-3" href="/cast/create">Tambah Cast</a>
<table id="example1" class="table table-bordered table-striped">
  <thead>
  <tr>
    <th>ID</th>
    <th>Nama</th>
    <th>Umur</th>
    <th>Bio</th>
    <th>Aksi</th>
  </tr>
  </thead>
  <tbody>
  @forelse ($cast as $key=>$value)
  <tr>
    <td>{{$value->id}}</td>
    <td>{{$value->nama}}</td>
    <td>{{$value->umur}}</td>
    <td>{{$value->bio}}</td>
    <td>
        <form action="/cast/{{$value->id}}" method="POST">
          @csrf
          @method('delete')
        <a class="btn btn-success btn-xs" href="/cast/{{$value->id}}">Show</a>
        <a class="btn btn-warning btn-xs" href="/cast/{{$value->id}}/edit">Edit</a>
        <input type="submit" class="btn btn-danger btn-xs" value="Delete">
        </form>
    </td>
  </tr>
  @empty
  <tr colspan="4">
      <td>No data</td>
  </tr>  
  @endforelse   
  </tbody>        
  </table> 
@endsection

@push('scripts')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush
