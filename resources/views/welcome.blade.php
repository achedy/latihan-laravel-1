@extends('adminlte.master')

@section('title')
  Welcome
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item"><a href="/register">Register</a></li>
<li class="breadcrumb-item active">Welcome</li>
@endsection

@section('content')
    <span class="font-weight-bold">Selamat Datang!! </span> {{$firstname}} {{$lastname}}
    <p>Terimakasih telah bergabung di website kami. Selamat Belajar!</p>  
@endsection