# Belajar Laravel 1
*by Edy santoso* 

## About
Ini adalah Tugas Laravel 1 di PKS Digital School batch 2

Tugas ini meliputi :

### 1. Pembuatan route pada /routes/web.php
### 2. Pembuatan Controller pada /app/Http/Controller
<code>php artisan make:controller NamaController</code> 
### 3. Pembuatan template view di /resources/views/ 

**Pada content :**\
@yield('content') <- pada template  \
@extends('template') <- pada view 

@section('content') \
isi content \
@endsection

**Pada script :** \
@stack <- pada template

@push \
isi script \
@endpush

### 4. Penggunaan Datatables 

## Laravel License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).


