<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cast;


class CastController extends Controller
{
    
    public function Index()
    {
        $cast = Cast::all();
        return view('cast.index', compact('cast'));
    }

    public function Show($id){
        $show = Cast::find($id);
        return view('cast.show', compact('show'));   
    }

    public function Create(){
        return view('cast.create');
    }

    public function store(Request $request){

        $this->validate($request,[
    		'nama' => 'required',
    		'umur' => 'required',
            'bio' => 'required'
    	]);

        Cast::create([
    		'nama' => $request->nama,
    		'umur' => $request->umur,
            'bio' => $request->bio
    	]);
 
    	return redirect('/cast/create');
    }

    public function edit($id){
        $show = Cast::find($id);
        return view('cast.edit', compact('show'));
    }

    public function Update(Request $request){

        $this->validate($request,[
    		'nama' => 'required',
    		'umur' => 'required',
            'bio' => 'required'
    	]);

        $cast = Cast::find($request->id);
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->update();
        return redirect('/cast/'.$request->id.'/edit');
    }

    public function Destroy($id){
        $cast = Cast::find($id);
        $cast->delete();
        return redirect('/cast');
    }



}
