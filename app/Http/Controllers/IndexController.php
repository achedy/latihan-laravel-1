<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function Index(){
        return view('home');
    }

    public function Datatable(){
        return view('data-table');
    }
}
