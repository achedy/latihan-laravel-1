<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CastController;
use App\Http\Controllers\IndexController;
//use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Route; 

Route::get('/', 'IndexController@Index');
Route::get('/data-table', 'IndexController@Datatable');

Route::get('/register', 'AuthController@Register');
Route::post('/welcome', 'AuthController@Welcome');

// CRUD cast

// menampilkan list data para pemain film (boleh menggunakan table html 
Route::get('/cast', 'CastController@Index');
// menampilkan form untuk membuat data pemain film baru
Route::get('cast/create', 'CastController@Create');
//  menyimpan data baru ke tabel Cast
Route::post('/cast/store','CastController@Store');
// menampilkan detail data pemain film dengan id tertentu
Route::get('/cast/{cast_id}','CastController@Show');
// menampilkan form untuk edit pemain film dengan id tertentu
Route::get('/cast/{cast_id}/edit','CastController@Edit');
// menyimpan perubahan data pemain film (update) untuk id tertentu
Route::put('/cast','CastController@Update');
// 	menghapus data pemain film dengan id tertentu
Route::delete('/cast/{cast_id}','CastController@Destroy');




